### Readme
RNN policy to control a robotic arm (Kuka LWR4+) for HRI. Policy is learned from human-human interaction data.

#### System
- Ubuntu 16.04
- ROS Kinetic
- V-REP 3.5.0
- Anaconda (Python 2.7) - see reqs.txt for required packages

#### Insallation
1. Install ROS Kinetic (standard: http://wiki.ros.org/kinetic/Installation/Ubuntu)
2. Install catkin tools (http://catkin-tools.readthedocs.io/en/latest/installing.html) - for using _catkin build_ (catkin_make should also be ok)
3. Download Anaconda (version should not be important: https://www.anaconda.com/download/#linux)
    - Do **NOT** include anaconda path in your $PATH yet. Do **NOT** let the installation to put the _export_ statement in your _bashrc_ or _zshrc_ file.
4. Create a ROS workspace (standard, e.g. __rnn_ws__)
5. Clone this repo to your workspace source folder, e.g. __rnn_ws/src__
    - also download the _Data_ folder from **provide data repository url**
    - and put the _Data_ folder under __src/ros_rnn/src/module__
    - (need to remove this req.) create a directory called **include** under __ros_rnn__ folder.
6. Build the ws - __catkin build__ (note that ros workspace is built with ros-kinetic in the PATH not anaconda)
7. Source the ws - __source devel/setup.(bash/zsh)__ depending on your shell
8. Export anaconda path, e.g. 
    - set an alias in your bash/zsh: __alias useconda='export PATH="$HOME/anaconda3/bin:$PATH"'__ (mind the anaconda installation folder)
    - $ _useconda_ (so anaconda is exported)
    - now check _which python_ to verify the python path is from anaconda
    - if you had already installed anaconda, you might want to update it (probably not necessary):
        + **conda update -n root conda**: update the conda tool
        + **conda update -n root conda**: update the anaconda installation
9. Create a conda environment to isolate python packages specific to this project, e.g. __conda create -n rnn_kuka python=2.7__ 
10. Source the environment, e.g. __source activate rnn_kuka__
11. Install the required packages listed in 'src/reqs.txt'. Order suggestion: 
    - Check first that you are in the conda env!
    - then update pip, __conda update pip__ 
    - install/update setuptools, __conda install setuptools__
    - install/update catkin related packages: __sudo apt-get install python-catkin-tools__ (can be done before the env creation)    
    - install updated ros related packages (This step seems to be important!): __pip install -U rosdep rosinstall_generator wstool rosinstall six vcstools__ and run _rosdep update_
    - then the remaining RNN implementation related packages, probably starting with _tensorflow_ (checked with tf v1.8 as can be seen in the _reqs.txt_ file), _scipy, matplotlib, seaborn, etc._
12. Go to the workspace root folder, e.g. __rnn_ws/__
13. Build the ws just to make sure, __catkin build__ (note that anaconda is in the PATH now, but since it had initially been built with ros-kinetic python bindings this seems to be fine)
14. Source the newly build ws, __source devel/setup.(bash/zsh)__
15. Go to the folder, e.g. __cd src/ros_rnn/src/module__ 
16. On a separate terminal, source ws (this might not be necessary) and run _roscore_ (assuming everything runs locally, the roscore runs on localhost)
17. Run V-REP by running the script _./vrep.sh_ under the vrep root folder
18. Load scene named __kuka_ik.ttt__ which is under the folder __src/ros_rnn/src/module__ and _Play_ the simulation (now the kuka model is ready to receive end-effector control commands)
19. Run the code: __rosrun hri_rnn ros_interface__ 
20. Kuka should move to target and come back to initial end-effector position

anything missing??? 
