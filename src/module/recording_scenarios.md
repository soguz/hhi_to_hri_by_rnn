1. _goal reaching_: human->static, goal->static (just showing a simple 
avoidance and goal reaching)
2. _HRI cases_: human->dynamic, goal->st (2-3 cases similar to our ontology cases 1-3)
3. _HRI dynamic goal change_: human->dyn, goal->dyn (change the goal as if the robot realizes human is obstructing the path, in a way like action selection. no claim that we are doing this, it's just about that it can be doable/feasible.)
4. _High-5/Handover scenario_: human hand (dyn) is the goal
5. _Sequential goals_ (will check this one, not so sure): e.g., following the corners of a square path to show it doesn't have to be just pick-and-place with same start-goal pair. Actually this is similar to point 3.