from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections
import hashlib
import numbers

from tensorflow.python.framework import constant_op
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.framework import tensor_util
from tensorflow.python.layers import base as base_layer
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import clip_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn_ops
from tensorflow.python.ops import partitioned_variables
from tensorflow.python.ops import random_ops
from tensorflow.python.ops import variable_scope as vs
from tensorflow.python.ops import variables as tf_variables
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.util import nest

import numpy as np
import tensorflow as tf
from module.postprocessing_functions import *
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import rnn_cell_impl

# This file contains model related functions
def rnnCell(no_units, f_bias, reusePar, type="Basic", act_fcn = None):
    # Builds an instance of an RNN Cell according to the specifications
    # no_units:         Number of neurons in each cell/layer
    # f_bias:           Forget bias value
    # reusePar:         Tensorflow reuse parameter
    # type:             Cell type
    if type == "Basic":
        return tf.contrib.rnn.BasicLSTMCell(no_units, forget_bias = f_bias, state_is_tuple=True, reuse=False)
    elif type == "GRU":
        if act_fcn == None:
            # In case no customized activation function is used for GRU (default: tanh)
            return tf.contrib.rnn.GRUCell(no_units, reuse=False)
        elif act_fcn == "triple_tanh":
            # In case a customized activation function is used for GRU
            return tf.contrib.rnn.GRUCell(no_units, activation = triple_tanh, reuse=False)
        else:
            raise ValueError('Unknown activation function')
    elif type == "SimpleRNN":
        return tf.contrib.rnn.BasicRNNCell(no_units, reuse=False)
    elif type == "UGRNN":
        if act_fcn == None:
            return tf.contrib.rnn.UGRNNCell(no_units, reuse=False)
        elif act_fcn == "triple_tanh":
            # In case a customized activation function is used for GRU
            return tf.contrib.rnn.UGRNNCell(no_units, activation = triple_tanh, reuse=False)
        else:
            raise ValueError('Unknown activation function')
    elif type == "LSTM":
        return tf.contrib.rnn.LSTMCell(no_units, use_peepholes = True, reuse=False)
    else:
        return tf.contrib.rnn.BasicLSTMCell(no_units, forget_bias = f_bias, state_is_tuple=True, reuse=False)

def cost_function(prediction, targets, time_steps):
    # Implements a cost function with mean squared error (MSE)
    # prediction:       Predicted tensor
    # targets:          Original tensor
    # time_steps:       Number of relevant time steps in tensor
    c = tf.reduce_sum(tf.multiply(prediction-targets, prediction-targets))/\
    tf.cast(tf.reduce_sum(time_steps), dtype=tf.float64)
    tf.Print(c, [c, "Hello"])
    return c

def numericalIntegration(tensor_series, time_steps, batch_size, dimensions, mask, delta_T):
    # This function implements numerical integration with the trapezoidal rule
    # tensor_series:    sequence(s) to integrate
    # time_steps:       Maximum number of time steps in batch/sequences
    # batch_size:       Number of sequences, batch size
    # dimensions:       Dimensionality of each single sequence
    # mask:             Indicates which values are no longe parte of the integration
    init_step = tensor_series[:,0,:] + 0.5*(tensor_series[:,1,:]-tensor_series[:,0,:])
    integral = tf.reshape(init_step, [batch_size,1,dimensions])
    for idx in xrange(1,time_steps-1,1):
        new_t_step = tf.reshape(integral[:,idx-1,:] + 0.5*(tensor_series[:,idx+1,:]-tensor_series[:,idx,:]), [batch_size,1,dimensions])
        integral = tf.concat( [integral, new_t_step], axis=1 )
    integral = integral*delta_T
    integral = tf.multiply(integral, mask)
    return integral

def generate_model_name(opt_method, lay_list, unit_list, data_set="rep_rot"):
    # This function generates a (unique) model name, e.g. for saving checkpoints
    # opt_method:       Optimizer method that is used
    # lay_list:         List of the layer configuration (c.f. config file)
    # unit_list:        List of number of units/neurons per layer (c.f. config file)
    # data_set:         Suffix describing a data_set
    mod_name = opt_method
    for idx in range(len(lay_list)):
        mod_name = mod_name + "_" + lay_list[idx] + str(unit_list[idx])
    mod_name = mod_name + "_" + data_set
    return mod_name

def triple_tanh(x):
    # This is a modified activation function that addresses a three-phase approach:
    #
    # x:                The input tensor
    #
    # Math: modified_activation = 
    # (tanh(scaling*(x-offset_1)) + tanh(scaling*(x-offset_2)) + tanh(scaling*(x-offset_3)))/3
    offset_1 = -2.0
    offset_2 = 0.0
    offset_3 = -offset_1
    scaling = 4.0
    modified_activation = tf.divide( tf.tanh( tf.scalar_mul(scaling, x - offset_1) ) + \
        tf.tanh( tf.scalar_mul(scaling, x - offset_2) ) + \
        tf.tanh( tf.scalar_mul(scaling, x - offset_3) ), 3.0 )
    return modified_activation

def scipy_opt_loss_callback(loss_tensor, config):
    file_n = "Comparison/" +  config  + "_loss_values_scipy.csv"
    with open(file_n, "a") as myfile:
        myfile.write(str(loss_tensor) + "\n")
