########################################
#            ROS Interface             #
########################################

# Additional python functions:
# Import from custom files
from data_functions import *
from model_functions import *
from postprocessing_functions import *
from beta_new_modular_functions import *
from rnn_wrapped_object import *
from rnn_wrapped_object import trained_rnn_object


# Import from shipped libraries
import scipy.io as sio
import numpy as np
import tensorflow as tf

import matplotlib.pyplot as plt

# import ros libraries
import rospy
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Point
from qualisys.msg import Subject

# import statistics


import datetime
import time

import sys
import ConfigParser
import os
from math import sin, cos
import numpy.linalg
from math import pi as PI
#Class
class rosinterface(object):

	def __init__(self, sess, goal_pos = np.array([-8.8360e-1, -8.2930e-1, +1.0400e+0])):	#[-1150, -850, 1400] for vrep
		# self.joint_ang_seq = np.array([[80.44 * PI /180, 17.22 * PI / 180, 0, -70.36 * PI /180, 0, 0 ,0]]) # Sequence of joint angles
		self.scenarioID = 2
		deg2rad = PI/180.
		self.joint_ang_seq = np.array([[+1.500e+1*deg2rad, -5.500e+1*deg2rad, +1.900e+2*deg2rad, +7.500e+1*deg2rad, \
										+6.500e+1*deg2rad, +3.000e+1*deg2rad, +1.500e+1*deg2rad]]) # zero-config: joint angles
		self.wrist_position = np.array([-1.4085e+0, -5.5445e-1, +9.9427e-1])
							# = self.getEndEffectorPosition(self.joint_ang_seq[-1])	# init ee-pos in m for non-rnn processes
		self.obstacle_pos = np.array([0, 0, 0])
		self.goal_pos =  goal_pos  # Define goal_ position !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
		print('q: ', self.joint_ang_seq)
		print('w: ', self.wrist_position)
		print('g: ', self.goal_pos)
		self.pub = None
		self.humanhand_pub = None
		self.new_pos = None
		self.back_pos = None
		self.back_ang = None	# for return joint angle traj
        # a list contains real trajectory to publish
		self.traj = []

	    # sequence from subscriber  
		self.acc_seq = None # Sequence of end effector acceleration output values
		self.vel_seq = None # Sequence of end effector acceleration based velocity values
		self.loc_seq = None # Sequence of end effector velocity based position values

		self.sess = sess  # Tensorflow session, needs to be passed

		self.idx = 0

		self.msg_positions = Float64MultiArray()
		self.msg_positions.data = np.zeros(7)
		self.msg_gains = Float64MultiArray()
		self.msg_gains.data = np.zeros(14)
		self.msg_torque = Float64MultiArray()
		self.msg_torque.data = np.zeros(7)
		self.initRosMsgs()

		config_file, config_path, config, file_name, decay, initial_learning_rate, batches_per_epoch, num_epochs, \
		decay_start_epoch, bs, num_samples, load_results, is_training, load_existing_graph, optimizer_method, \
		regularize_weights, regularize_w_const, regularize_acc, regularize_a_const, num_layers, target_dimensions, \
		input_dimensions, max_ts, layer_list, neuron_list, delta_t, activation_fcn, rep_dist, plotting, num_ts_simulation, \
		test_files, save_test_files, forward_indices, backward_indices, reaching_threshold, add_noise, static_simulation, \
		saver_path, no_sam, iD, tD, lD, mx_in, mn_in, mx_out, mn_out, wrist, obstacle, goal, tensor_name_dict \
		= load_information_for_rnn_tests(['', 'config_9.ini']) # (sys.argv)


    	# Here we initialize a new object of the class "trained_rnn_object" with all associated value
		self.new_rnn = trained_rnn_object(max_in = None, min_in = None, max_out = None, min_out = None, model_path = "checkpoints/rf20_64_fix40/model", batch_size=10, sess=sess, \
			tensor_name_dict = {'x': "Placeholder:0", 'y': "Placeholder_1:0", 'outputs': "add:0", \
			'states': ['rnn/while/Exit_2:0', 'rnn/while/Exit_3:0', 'rnn/while/Exit_4:0', 'rnn/while/Exit_5:0'], \
			'init_states': ['MultiRNNCellZeroState/BasicLSTMCellZeroState/zeros:0', 'MultiRNNCellZeroState/BasicLSTMCellZeroState/zeros_1:0', \
			'MultiRNNCellZeroState/BasicLSTMCellZeroState_1/zeros:0', 'MultiRNNCellZeroState/BasicLSTMCellZeroState_1/zeros_1:0']}, \
			mode = 'scale', scale = 1000.0, Ts=0.032, input_dim=6, output_dim=3, rep_dist=400.0)

		#self.new_rnn = trained_rnn_object(max_in = mx_in, min_in = mn_in, max_out = mx_out, min_out = mn_out, model_path = "checkpoints/***/my_save_file", 
		#	batch_size=1, sess=sess, tensor_name_dict = tensor_name_dict, mode = 'map', scale = None, Ts=delta_t, input_dim=input_dimensions, output_dim=target_dimensions, rep_dist=rep_dist)


############################
##          ROS           ##
############################

	def callObstacle(self, obstacle_msg):
		# rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data) # not important
		# self.obstacle_pos = obstacle_msg.pose.position
		self.obstacle_pos = np.array([obstacle_msg.x, obstacle_msg.y, obstacle_msg.z])
		# pass
		# print('accept new human wrist position: ', self.obstacle_pos)
	
	def callQualHand(self, hand_msg):
		# rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data) # not important
		# self.obstacle_pos = obstacle_msg.pose.position
		self.obstacle_pos = np.array([hand_msg.position.x, hand_msg.position.y, hand_msg.position.z])
		# pass
		# print('accept new human wrist position: ', self.obstacle_pos)


	def callWrist(self, wrist_msg):
		# rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data) # not important 
		# self.wrist_position = wrist_msg.pose.position
		self.wrist_position = np.array([wrist_msg.x, wrist_msg.y, wrist_msg.z])	# was for vrep
		# self.wrist_position = np.array([wrist_msg.pose.position.x, wrist_msg.pose.position.y, wrist_msg.pose.position.z])*1000	# mm for kuka and new msg type
		# print('accept new robot end-effector position: ', self.wrist_position)

	def cbGoalPos(self, goal_msg):
		# dist = numpy.linalg.norm(self.goal_pos - [goal_msg.x, goal_msg.y, goal_msg.z])
		# if dist > 0.05:
		# 	rospy.sleep(1.0)	# goal changed, let's wait 1 sec.
		self.goal_pos = np.array([goal_msg.x, goal_msg.y, goal_msg.z])	# from vrep goalSphere object


	# Subscriber for NN
	def get_pos(self ):

		rospy.init_node('integration', anonymous=True) 

		if self.scenarioID == 4:	# if high-5/handover
			rospy.Subscriber("/wristForVrep", Point, self.cbGoalPos)	# wrist of human becomes the goal
		else:
			# rospy.Subscriber("/wristForVrep", Point, self.callObstacle)	# wrist of human is now an obstacle - that is for vrep without qual
			# rospy.Subscriber("/standing2/kuka_lwr_state_controller/msr_cart_pose", PoseStamped, self.callWrist) # this is directly from kuka
			rospy.Subscriber("/goal_pos", Point, self.cbGoalPos) 	# oz: goal pos subs from vrep
			rospy.Subscriber("/qualisys/wrist_right_3", Subject, self.callQualHand) 	# oz: goal pos subs from vrep

		rospy.Subscriber("/ee_pos", Point, self.callWrist) 		# oz: kuka-ee pos subs, directly from vrep

		rospy.sleep(0.1)	# maybe we don't need this - 2 init_nodes before


	# Publisher that publishes the new wrist_positionition 
	def set_pos(self):
		self.pub = rospy.Publisher('new_pos', Point, queue_size=10)
		self.humanhand_pub = rospy.Publisher('wristForVrep', Point, queue_size=10)
		## for joint impedance control directly on kuka
		# pub_positions = rospy.Publisher("/standing2/joint_impedance_controller/position", Float64MultiArray, queue_size=50);
		# pub_gains = rospy.Publisher("/standing2/joint_impedance_controller/gains", Float64MultiArray, queue_size=50);
		# pub_torque = rospy.Publisher("/standing2/joint_impedance_controller/torque", Float64MultiArray, queue_size=50);
		# rospy.init_node('integration', anonymous=True)	# do we need this - check get_pos already has it

		if self.scenarioID == 1:	# goal reaching:
			self.runScenario1()
		elif self.scenarioID == 2:	# HRI cases
			self.runScenario2()
		elif self.scenarioID == 3:	# HRI dynamic goal change
			self.runScenario3()
		elif self.scenarioID == 4:	# high-5/handover scenario
			self.runScenario4()
		elif self.scenarioID == 5:	# Sequential goals
			self.runScenario5()
		else:
			print('NO SUCH SCENARIO!')	


	# goal reaching: human->static, goal->static (just showing a simple avoidance and goal reaching)
	def runScenario1(self):
		pass


	# HRI cases: human->dynamic, goal->st (2-3 cases similar to our ontology cases 1-3)
	def runScenario2(self):
		rate = rospy.Rate(16) # 
		time_1 = datetime.datetime.now()
		time_2 = datetime.datetime.now()
		time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	
		start_flag = False

		while time_delta < 90000 and (not rospy.is_shutdown()):
			g = self.goal_pos*1000			# mm for rnn
			w = self.wrist_position*1000	# mm for rnn
			o = self.obstacle_pos*1000		# mm for rnn

			if o[0] < -1450 and o[1] < -293 and not start_flag:
				time_1 = datetime.datetime.now()	# do not start the timer yet
				continue

			start_flag = True 	# do not check start condition once again

			self.new_rnn.step(o, g, y = None, wristPos = w, idx=self.idx, numerical_integration = True, test_exception=True) ###### idx_2
			accs, vels, locs = self.new_rnn.get_sequences()  
			# # print('locs_seq:', locs)
			self.acc_seq = accs              # Sequence of end effector acceleration output values
			self.vel_seq = vels              # Sequence of end effector acceleration based velocity values
			self.loc_seq = locs/1000		 # convert to m, from rnn to env
			self.idx += 1


			self.new_pos = self.loc_seq[0,-1,:]
			self.traj.append(self.new_pos)	
			# print(self.new_pos)
			pos_msg = Point() # position
			pos_msg.x = self.new_pos[0]
			pos_msg.y = self.new_pos[1]
			pos_msg.z = self.new_pos[2]  
			self.pub.publish(pos_msg)

			hand_msg = Point()
			hand_msg.x = self.obstacle_pos[0]
			hand_msg.y = self.obstacle_pos[1]
			hand_msg.z = self.obstacle_pos[2]
			self.humanhand_pub.publish(hand_msg)
			rate.sleep()

			time_2 = datetime.datetime.now()
			time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		# print("DONE!!!")


	# HRI dynamic goal change: human->dyn, goal->dyn (change the goal as if the 
	# robot realizes human is obstructing the path, i.e. action selection.
	def runScenario3(self):
		rate = rospy.Rate(16) # 
		time_1 = datetime.datetime.now()
		time_2 = datetime.datetime.now()
		time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		while time_delta < 10000 and (not rospy.is_shutdown()):
			g = self.goal_pos*1000			# mm for rnn
			w = self.wrist_position*1000	# mm for rnn
			o = self.obstacle_pos*1000		# mm for rnn

			self.new_rnn.step(o, g, y = None, wristPos = w, idx=self.idx, numerical_integration = True, test_exception=True) ###### idx_2
			accs, vels, locs = self.new_rnn.get_sequences()  
			self.acc_seq = accs              # Sequence of end effector acceleration output values
			self.vel_seq = vels              # Sequence of end effector acceleration based velocity values
			self.loc_seq = locs/1000		 # convert to m, from rnn to env
			self.idx += 1


			self.new_pos = self.loc_seq[0,-1,:]
			self.traj.append(self.new_pos)	
			pos_msg = Point() # position
			pos_msg.x = self.new_pos[0]
			pos_msg.y = self.new_pos[1]
			pos_msg.z = self.new_pos[2]  
			self.pub.publish(pos_msg)
			rate.sleep()

			time_2 = datetime.datetime.now()
			time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		# print("DONE!!!")


	# high-5/handover scenario 
	def runScenario4(self):
		rate = rospy.Rate(32) # 
		time_1 = datetime.datetime.now()
		time_2 = datetime.datetime.now()
		time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		while time_delta < 2500 and (not rospy.is_shutdown()):
			g = self.goal_pos*1000			# mm for rnn
			w = self.wrist_position*1000	# mm for rnn
			o = self.obstacle_pos*1000		# mm for rnn

			self.new_rnn.step(o, g, y = None, wristPos = w, idx=self.idx, numerical_integration = True, test_exception=True) ###### idx_2
			accs, vels, locs = self.new_rnn.get_sequences()  
			self.acc_seq = accs              # Sequence of end effector acceleration output values
			self.vel_seq = vels              # Sequence of end effector acceleration based velocity values
			self.loc_seq = locs/1000         # convert to m, from rnn to env
			self.idx += 1

			self.new_pos = self.loc_seq[0,-1,:]
			self.traj.append(self.new_pos)	
			pos_msg = Point() # position
			pos_msg.x = self.new_pos[0]
			pos_msg.y = self.new_pos[1]
			pos_msg.z = self.new_pos[2]  
			self.pub.publish(pos_msg)

			rate.sleep()

			time_2 = datetime.datetime.now()
			time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		# print("DONE!!!")

	# Sequential goals
	def runScenario5(self):
		rate = rospy.Rate(32) # 
		time_1 = datetime.datetime.now()
		time_2 = datetime.datetime.now()
		time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		while time_delta < 11000 and (not rospy.is_shutdown()):
			g = self.goal_pos*1000			# oz: mm for rnn
			w = self.wrist_position*1000	# oz: mm for rnn
			o = self.obstacle_pos*1000		# oz: mm for rnn

			self.new_rnn.step(o, g, y = None, wristPos = w, idx=self.idx, numerical_integration = True, test_exception=True) ###### idx_2
			accs, vels, locs = self.new_rnn.get_sequences()  
			self.acc_seq = accs # Sequence of end effector acceleration output values
			self.vel_seq = vels # Sequence of end effector acceleration based velocity values
			self.loc_seq = locs/1000		# convert to m, from rnn to env
			self.idx += 1
			
			self.new_pos = self.loc_seq[0,-1,:]
			self.traj.append(self.new_pos)	
			pos_msg = Point() # position
			pos_msg.x = self.new_pos[0]
			pos_msg.y = self.new_pos[1]
			pos_msg.z = self.new_pos[2]  
			self.pub.publish(pos_msg)
			rate.sleep()

			time_2 = datetime.datetime.now()
			time_delta = (time_2 - time_1).microseconds/1000.0 + (time_2 - time_1).seconds*1000.0	

		# print("DONE!!!")


	def initRosMsgs(self):
		for i in range(7):
			self.msg_gains.data[i] =  700;
			self.msg_gains.data[i+7] = 0.6;
			self.msg_torque.data[i] = 0;
			